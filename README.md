# Inkscape extension number subpaths

Number subpaths to visualize the order and show start/end node with green/red dot.

When working with combined paths the order of the paths is not easily visible in Inkscape. The extension is based on the Visualize Path > Number Nodes extension by Aaron Spike from 2005. 

I find this extension helpful for various tasks; while working with embroidery designs where the path order is essential and when developing and testing new extensions or designs for cutting/plotting.

The extension can be used with the Live preview to avoid adding the visual elements to the design. If the visual elements are added to the design these will be grouped and thus easily removed.

Download the .inx and .py file and place these in your local extensions folder. Restart Inkscape and the extension can be found in Extensions > Visualize Path > Number Subpaths
